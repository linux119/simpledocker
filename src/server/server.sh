#!/bin/bash
all="gcc server.c -o s21_server $(pkg-config --cflags --libs fcgi)"
clean="rm -rf s21_server"
spawn="spawn-fcgi -a 127.0.0.1 -p 8080 ./s21_server"
stop="sudo kill $(ps -e | grep s21_server | awk '{print $1}')"
check="echo $(ps -e | grep s21_server)"
nginx_conf="sudo cp nginx.conf /etc/nginx/nginx.conf && (nginx -s reload || nginx)"
nginx_kill="sudo kill $(cat /var/run/nginx.pid 2>/dev/null)"
nginx_user="sudo useradd -s /bin/false nginx"
restart="$stop && $clean && $all && $spawn"
start="$clean && $all && $spawn"
eval ${!1}