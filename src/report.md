## Part 1. Готовый докер
* ```docker pull nginx```  
     ![](img/1-1.PNG)  
     Взять официальный докер образ с nginx и выкачать его при помощи docker pull
* ```docker images```  
     ![](img/1-2.PNG)  
    Проверить наличие докер образа через docker images
* ```docker run -d nginx```  
     ```docker ps```  
     ![](img/1-3.PNG)  
     Запустить докер образ через docker run -d [image_id|repository]  
     Проверить, что образ запустился через docker ps
* ```docker inspect [container_id|container_name]```  
     ![](img/1-size.PNG)  
     ![](img/1-prots.PNG)  
     ![](img/1-ip.PNG)  
     Посмотреть информацию о контейнере через docker inspect [container_id|container_name]  
     По выводу команды определить и поместить в отчёт размер контейнера, список замапленных портов и ip контейнера
* ```docker stop docker inspect [container_id|container_name]```  
     ```docker ps```  
     ![](img/1-stop.PNG)  
     Остановить докер образ через docker stop [container_id|container_name]  
     Проверить, что образ остановился через docker ps
* ```docker run -p 80:80 -p 443:443 -d nginx```  
     ![](img/1-80-443.PNG)  
     Запустить докер с замапленными портами 80 и 443 на локальную машину через команду run
* ![](img/1-browser.PNG)  
     Проверить, что в браузере по адресу localhost:80 доступна стартовая страница nginx
* ```docker run -d nginx```  
     ![](img/1-restart.PNG)  
     Перезапустить докер контейнер через docker restart [container_id|container_name]  
     Проверить любым способом, что контейнер запустился

## Part 2. Операции с контейнером
* ![](img/2-1.PNG)  
     Прочитать конфигурационный файл nginx.conf внутри докер образа через команду exec
* ![](img/2-2.PNG)  
     Создать на локальной машине файл nginx.conf  
     Настроить в нем по пути /status отдачу страницы статуса сервера nginx
* ![](img/2-3.PNG)  
     Скопировать созданный файл nginx.conf внутрь докер образа через команду docker cp  
     Перезапустить nginx внутри докер образа через команду exec
* ![](img/2-4.PNG)  
     Проверить, что по адресу localhost:80/status отдается страничка со статусом сервера nginx
* ![](img/2-5.PNG)  
     Экспортировать контейнер в файл container.tar через команду export  
     Остановить контейнер  
     Удалить образ через docker rmi [image_id|repository], не удаляя перед этим контейнеры  
     Импортировать контейнер обратно через команду import
* ![](img/2-6.PNG)  
     Запустить импортированный контейнер
